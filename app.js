const express = require('express');
const redis = require('redis');

// Create a Redis client
const client = redis.createClient({
  host: '127.0.0.1', // This should match the hostname of your Redis container
  port: 6379 // This should match the exposed Redis port in the Dockerfile
});

// Connect to Redis
client.on('connect', () => {
  console.log('Connected to Redis');
});

// Create an Express application
const app = express();

// Define a route for the index page
app.get('/', (req, res) => {
  // Increment the counter in Redis
  client.incr('visitorCount', (err, count) => {
    if (err) {
      console.error(err);
      res.status(500).send('Error fetching counter from Redis');
    } else {
      // Display the counter on the webpage
      const deployedEnv = process.env.DEPLOYED_ENV || 'unknown';
      res.send(`This is the ${count} visitor from environment ${deployedEnv}. SemVer V1.8`);
    }
  });
});

// Start the Express server
app.listen(80, () => {
  console.log('Server started on port 80');
});
