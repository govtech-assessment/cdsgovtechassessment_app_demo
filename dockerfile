# Use the official Node.js image as the base
FROM node:current-alpine3.18

# Set the working directory in the container
WORKDIR /app

# Copy the package.json and package-lock.json files to the container
COPY package*.json ./

# Install the application dependencies
RUN npm install --production

RUN apk --update add redis 

# Copy the application code to the container
COPY . .

# Expose the port that the application listens on
EXPOSE 80

# Start the Node.js application
CMD redis-server --daemonize yes && node app.js
